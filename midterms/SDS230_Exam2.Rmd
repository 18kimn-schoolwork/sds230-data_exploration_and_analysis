---
title: "Exam 2"
author: "Nathan Kim"
date: 'April 17, 2020'
output:
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
options(scipen = 999)
```

*****************************

All of the questions on this exam relate to data on firearm fatalities by state as well as other legislative and demographic information by state.  If you're curious, the data is drawn from

https://www.statefirearmlaws.org/table.html
https://www.statefirearmlaws.org/glossary.html
https://en.wikipedia.org/wiki/List_of_U.S._states_by_income
https://www.cdc.gov/nchs/pressroom/sosmap/firearm_mortality/firearm.htm

The code below will load necessary libraries and functions. 
```{r, message=FALSE}
library(car)
library(leaps)
library(lubridate)
library(rvest)
library(olsrr)
library(corrplot)
library(leaps)
source("http://www.reuningscherer.net/s&ds230/Rfuncs/regJDRS.txt")

library(tidyverse)
```

#### 1)  Data retrieval and creation

Modify the code below as indicated.
```{r}
#Get dataset
guns <- read.csv("http://reuningscherer.net/s&ds230/data/Guns_2017.csv", as.is = TRUE)
#Get dimension, top six rows, and variable names of guns

dim(guns)
head(guns)
names(guns)

```

Here is information on some of the variables

* rate_16 : firearm fatality rate per 100,000 in 2016
* median.in : median income in 2015 per household (dollars)
* lawtotal : total number of laws regulating firearms in a state
* LawRanking : ranking (1 = best, 50= worst) from Giffords Center on gun laws in the state
* GiffordsGrade : Grading of gun laws in a state (A, A-, B+, etc.)

The other variables you can learn about [HERE](https://www.statefirearmlaws.org/resources) (you have to download the excel file and look at the codebook sheet)

The code below scrapes data from http://www.gunsandammo.com/second-amendment/best-states-for-gun-owners-2017/ to get state rankings of gun friendliness according to Guns and Ammo Magazine.  Nothing to do here, but you'll want to examine what each line of code does for your own edification.

```{r}
#Define the URL of interest
url <- "http://www.gunsandammo.com/second-amendment/best-states-for-gun-owners-2017/" 

#Read the HTML code from the website into a new object
webpage <- read_html(url) 

#Get rankings and strip off extraneous information.   Sort rankings so that they are in state alphabetical order so that you can merge this information onto the guns object.  Use whatever code you like for this.  The argument you want to use in the html_nodes function is 'p strong'

#Code here.   End up with code below

rankingHTML <- html_nodes(webpage,'p strong')
ranking <- html_text(rankingHTML)
ranking <- ranking[-c(1,52)]
statenm <- gsub(".*\\. ","",ranking)

#Create ammoRank variable.  This should be an INTEGER variable.
guns$ammoRank <- c(50:1)[order(statenm)]
guns$ammoRank

```

Change the code below to create a new variables called `Grade` on the guns object as indicated.  Also modify `median.inc` as indicated, and create a variable called `loglaws`.

```{r}

#Create new variable called Grade such that GiffordsGrade is recoded as follows (you may want to remind yourself how to use the recode function):
#  Any form of A or B becomes 'AB'
#  Any form of C or D becomes 'CD'
#  F stays at F

guns$Grade <- guns$GiffordsGrade %>% 
  str_replace_all("(A.*)|(B.*)", "AB" ) %>% 
  str_replace_all("(C.*)|(D.*)", "CD") 

#Change median income so that it is in thousands of dollars, not dollars.
guns$median.inc <- guns$median.inc/1000

#Create a variable that is the natural log of lawtotal.
guns$loglaws <- log(guns$lawtotal)

```

#### 2)  One Way ANOVA

The goal here is compare firearm deaths rates in 2016 based on your newly created grading system in `Grade`. Modify the code below as indicated to create the specified objects.  You'll notice I've filled in yes/no questions to help you get the format correct - however, the specific values of 'yes' or 'no' are not necessarily correct.

```{r}
#Create a boxplot of rate_16 by Grade.  Add a title and change the color. 

ggplot(guns, aes(x = Grade, y = rate_16)) + 
  geom_boxplot(color = "blue") + 
  theme_bw() + 
  labs(title = "Gifford's Grade of State Gun Laws vs Firearm Fatality Rate", 
       y = "Firearm Fatality Rate (Deaths per 100,000 pop., 2016)")
#Calculate the standard deviation of rate_16 for each grade group
sds <- by(guns$rate_16, guns$Grade, sd)

#Get ratio of max to min sample standard deviation by Grade and round to one decimal place.
max(sds)/min(sds) %>% round(1)

#Based on previous calculation, is the equal variance assumption of ANOVA reasonably met?  yes or no
(q104 <- "yes")

#Calculate a one-way ANOVA comparing mean rate_16 by Grade.  Use the aov() function.   
(mod1 <- aov(rate_16~Grade, data = guns))

#Get summary of mod1
summary(mod1)
#Create Tukey confidence intervals for all pairs of groups. 
tuk <- TukeyHSD(mod1)
#Plot Tukey Confidence intervals.
plot(tuk)
#Use the myResPlots2 function to evaluate model assumptions for mod1.
myResPlots2(mod1, label = "Firearm Fatality Rate~Gun Law Grades")
#Do the residuals seem approximately normally distributed?
#Does the plot of fits vs. residuals indicate any model lack of fit?
(q110 <- c("yes","no"))
#There appears to be a very small amount of heteroskedasticity, 
#but to what I consider an acceptable extent. 

#Perform a Kruskal-Wallis test to compare rate_16 based on Grade
kruskal.test(rate_16~Grade, data = guns)
#The test rejects the null hypothesis that these samples were generated from the 
#same distribution, i.e. it seems that there are differences between different 
# gun law grades and firearm fatality rates. 
```

#### 3)  Box Cox Transformations

The goal here is fit a model predicting the total laws in a state regulating guns based on median income and the Guns and Ammo ranking `ammoRank`, perform a Box-Cox analysis, determine an appropriate transformation.

```{r}
#Fit a regression predicting lawtotal based on median.inc and ammoRank
mod2 <- lm(lawtotal~median.inc + ammoRank, data = guns)

#Get summary information for model q211
summary(mod2)

#Run a Box Cox procedure on model q211
boxCox(mod2)

#What is a reasonable suggested value for lambda?  Replace 'lambda' below with a number.
(q214 <- c(0))

#Fit a regression model predicting the transformed version of lawtotal suggested by the Box Cox Transformation.
#  Again, median.inc and ammmoRank are the two predictors.
(mod3 <- lm(log(lawtotal)~median.inc + ammoRank, data= guns))

#Get summary information for model mod3.
summary(mod3)

#Are both predictors statistically significant at the 0.05 level?
(q217 <- c("yes"))

#According to the model mod3, as ammoRank increases, does the number of laws increase or decrease? 
(q218 <- c("increase"))

#use myResPlots2 to get residual plots for model mod3.  Discuss the fit of this model in a few sentences.  Any regression outliers?
myResPlots2(mod2)

```

*Residuals of states seem to be approximately normally distributed, but there is some heteroskedasticity and the assumption of zero conditional mean of residuals seems to have been violated (the errors are lower near the median of the fitted values). There appears to be one outlier from the quantile-quantile plot for errors, which (referncing the 5th row of the dataset) appears to be California; as California is the largest state by population and has a high person-of-color population compared to many states, it makes sense that it appears to be an outlier compared to other states.*


#### 4)Correlation and Scatterplots

The goal here is to create correlation plots and scatterplots (matrix plots) for the continuous variables in `guns`.

```{r fig.height = 5, fig.width = 5}

#create an object q301 that only has the following columns of guns : rate_16, median.inc, loglaws, LawRanking, ammoRank.
q301 <- guns %>% select(rate_16, median.inc, loglaws, LawRanking, ammoRank)

#Get correlations for the variables in q301 and round to two decimal places.
cor(q301) %>% round(2)

#Create an object that has the results of cor.mtest for the columns of q301.  Use 95% CI.
(q303 <- cor.mtest(q301, conf.level = .95))

#Use corrplot.mixed to display confidence ellipses, pairwise correlation values, and put on 'X' over non-significant values for the columns in q302.
corrplot.mixed(cor(q301), lower.col="black", upper = "ellipse", tl.col = "black", number.cex=.7, 
                tl.pos = "lt", tl.cex=.7, p.mat = q303$p, sig.level = .05)

#Are the correlations you observe relatively strong or relatively weak?
(q305 <- c("relatively strong"))

#Use the parisJDRS() function on the columns of q301.
(q306 <- pairsJDRS(q301))

```

#### 5) Perform Best Subsets Regression

The goal here is to perform best subsets regression to predict `rate_16`, determine the best model according to the Bayesian Information Criteria (BIC), and evaluate the model fit.


```{r, }

names(guns)
#Create a dataframe guns2 from guns that removes state, code, rate_05, lawtotal, GiffordsGrade, Grade.
guns2 <- guns %>% select(-state, -code, -rate_05,
                         -lawtotal, -GiffordsGrade, -Grade)

#Run bests subsets to predict rate_16 based on all other columns in guns2 - use regsubsets() function.
(q401 <- regsubsets(rate_16~., data = guns2, nvmax = ))

#Get summary information for q401
q402 <- summary(q401)

#Get the which matrix from the summary in the previous question.
(q403 <-  q402$which)

#If you were to fit a model with only one predictor, which predictor would this be? CHANGE TEXT BELOW.
(q404 <- c("loglaws"))

# Get the best model according to the Bayesian Information Criteria (BIC)
(q405 <- which.min(q402$bic))

#Which variables are in this model?  Can code from names or use quoted string.
names(guns2)[q402$which[q405, ]][-1]


#Fit the model suggested by BIC and save to q407
guntemp <- guns2 %>% select(rate_16, college, nosyg, loglaws)
(q407 <- lm(rate_16 ~ ., data = guntemp))

#Get summary info for model q407
summary(q407)
#Run myResPlots2 on model q407
myResPlots2(q407)

#Are the model assumptions reasonably met?
(q409 <- c("yes"))
```


####6) ANCOVA

Part 5) showed that one of the main predictors of firearm death rates is log(Number of Gun Laws).  We might wonder what predicts the number of gun laws in a state.  

The goal here is to model and visually assess the predictive ability of median income and whether or not a state has a stand your ground law (`nosyg`) on log(Number of Gun Laws).

```{r}

#Fit a model that predicts loglaws based on median.inc, nosyg AND the interaction of median.inc and nosyg.  Save model as q501.
q501 <- lm(loglaws~ median.inc + nosyg + median.inc*nosyg, data = guns)

#Get summary information for model q501
summary(q501)
#Run the Anova() function on model q501 - be sure to request type III errors.
Anova(q501, type = 3)
#Get coefficients from model q501
(q504 <- q501$coefficients)

#Make a plot of loglaws vs median income with different colors for each level of nosyg.  Then add the predicted regression lines (one for each level of nosyg).  Add plot labels, titles, a legend, etc.
attach(guns)
plot(loglaws ~ median.inc, col = factor(nosyg), pch = 16, cex = .5)
legend("topleft", col = 1:5, legend = levels(factor(nosyg)), pch = 16)

abline(a = q504[1], b = q504[2], col = "black", lwd = 3)
abline(a = q504[1] + q504[3], b = q504[2] + q504[4], col = "red", lwd = 3)


```

Finally, write a few sentences interpreting the results of this model (which terms were significant, whether interaction was significant, how to interpret the results).

*At face value, it appears that median income negatively slightly negatively predicts logged number of gun laws, but this is not significant at an alpha = .05 level and only applies to states that have a stand-your-ground law. In fact, median income appears to be positively associated with loglaws for states that do not have a stand your ground law, with an significant p-value of p=.0003. The model evidence is supported graphically by graphing modeled trends over true values for median.inc and loglaws. `nosyg` appears to be significant negatively associated with loglaws, which suggests that states that have a stand your ground law have a lower number of gun laws, but again, this effect is heterogenous across median income and it can be seen graphically that for states that have a higher median income, a stand your ground law is associated with more gun laws, not less.  Finally, this model has an r-squared value of .5347, which implies that about 53% of the variation in log number of gun laws is explainable by median income, the presence of a stand your ground law, and their interaction.*

THE END



